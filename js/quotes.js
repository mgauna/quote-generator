/*jslint bitwise: true, plusplus: true, white: true*/
/*global $, Mustache, console*/
(function () {
  "use strict";
  /*QUOTES MODULE*/
  
  var quotes = {
    
    /*---MODULE VARIABLES---*/
    
    //Array of quotes
    quotes: [{quote: "You cannot not communicate.", author: "— PAUL WATZLAWIK"}, {quote: "If it doesn’t sell it isn’t creative.", author: "— DAVID OGILVY"}, {quote: "Nobody ever get fired for running an A/B test.", author: "— CHAD MAZZOLA"}, {quote: "If the bones don’t work, you’ve got a pile of skin.", author: "— SAMUEL ANTUPIT"}, {quote: "Sometimes magic is just someone spending more time on something than anyone else might reasonably expect.", author: "— RAYMOND JOSEPH TELLER"}, {quote: "Some designers create things to show you what they did. I design things to tell you what I solved.", author: "— BRIAN YERKES"}, {quote: "It is not daily increase but daily decrease; hack away the unessential.", author: "— BRUCE LEE"}, {quote: "I really like looking at design and thinking: that attention to detail must have taken absolutely ages.", author: "— SI SCOTT"}, {quote: "No masterpiece was ever created by a lazy artist.", author: "— SALVADOR DALI"}, {quote: "Simplicity should be found in the execution, not in the meaning.", author: "— NICHOLAS BURROUGHS"}, {quote: "Hackers don’t need idea people when they are the idea people.", author: "— MARC GRABANSKI"}, {quote: "Once the problem was stated, its solution came to me in a flash.", author: "— ANTON FOKKER"}, {quote: "The world always seems brighter when you’ve just made something that wasn’t there before.", author: "— NEIL GAIMAN"}, {quote: "It’s very easy to be different, but very difficult to be better.", author: "— JONATHAN IVE"}, {quote: "Moderation in all things, including moderation.", author: "— PETRONIUS"}, {quote: "Don’t stare at a blank page for too long, be bold, and make the first incisive stroke. The rest will come naturally.", author: "— JAMES KINGMAN"}, {quote: "Don’t let the truth get in the way of a good story", author: "— UNKNOWN"}, {quote: "Learning never exhausts the mind.", author: "— LEONARDO DA VINCI"}, {quote: "Give a crap. Don’t give a fuck.", author: "— KAREN MCGRANE"}, {quote: "Man up, admit it isn’t good enough, and fix it now.", author: "— ROBERT NEALAN"}, {quote: "Life beats down and crushes the soul, and art reminds you that you have one.", author: "— STELLA ADLER"}, {quote: "Good communication is as stimulating as black coffee, and just as hard to sleep after.", author: "— ANNE MORROW LINDBERGH"}, {quote: "Easy is not to be underestimated. Easy taps the pool of talent and ideas out there that were turned off by hard.", author: "— CHRIS ANDERSON"}, {quote: "Nail the basics first, detail the details later.", author: "— CHRIS ANDERSON"}, {quote: "Mistakes are the portals of discovery.", author: "— JAMES JOYCE"}, {quote: "Having small touches of colour makes it more colourful than having the whole thing in colour.", author: "— DIETER RAMS"}, {quote: "To create a memorable design you need to start with a thought that’s worth remembering.", author: "— THOMAS MANSS"}, {quote: "Always give a little more than you promise.", author: "— THE STUDEBAKER BROTHERS"}, {quote: "Because every person knows what he likes, every person thinks he is an expert on user interfaces.", author: "— PAUL HECKEL"}, {quote: "To some degree, to be creative you have to be selfish.", author: "— MEREDITH NORWOOD"}, {quote: "Do not seek to change what has come before. Seek to create that which has not.", author: "— DAVID AIREY"}, {quote: "A good design can feel like something you have always been waiting for without knowing.", author: "— PAUL HUIZINGA"}, {quote: "One should respect public opinion in so far as is necessary to avoid starvation and to keep out of prison.", author: "— BERTRAND RUSSELL"}, {quote: "Be regular and orderly in your life like a bourgeois, so that you may be violent and original in your work.", author: "— GUSTAVE FLAUBERT"}, {quote: "Best thing about creating something is that it starts living it’s own life.", author: "— HRISTO PANAYOTOV"}, {quote: "A hunch is creativity trying to tell you something.", author: "— FRANK CAPRA"}, {quote: "Do right.", author: "— JEFFREY BROWN"}, {quote: "The most effective way to make a design usable is to make it interesting enough that people want to use it.", author: "— BRYAN GREZESZAK"}, {quote: "Graphic design is the design of highly disposable items… It all winds up in the garbage.", author: "— KARRIE JACOBS"}, {quote: "Practicality is the serial killer of dreams.", author: "— RASHA HAMDAN"}, {quote: "Art does not reproduce what we see. Rather, it makes us see.", author: "— PAUL KLEE"}, {quote: "It’s OK to do stupid things, except when you notice them", author: "— KENNY SHOPSIN"}, {quote: "Design is the application of intent – the opposite of happenstance, and an antidote to accident.", author: "— ROBERT L. PETERS"}, {quote: "A designer who gives her art away, makes no pay. A designer who keeps her art to herself, makes no friends.", author: "— AMBER SEREE ALLEN"}, {quote: "The enemy of every author is not piracy, but obscurity.", author: "— CHRIS ANDERSON"}, {quote: "Designers shooting for usable is like a chef shooting for edible.", author: "— AARRON WALTER"}, {quote: "A lot of the things about design that tend to get designers really interested aren’t that important.", author: "— MICHAEL BIERUT"}, {quote: "If you do what you always did, you will get what you always got.", author: "— ALBERT EINSTEIN"}, {quote: "Never re-invent the wheel, unless you’re sure the client needn’t go anywhere.", author: "— RUSSELL BISHOP"}, {quote: "Design creates culture. Culture shapes values. Values determine the future.", author: "— ROBERT L. PETERS"}],
    
    //Last random index generated, needed to compare with the new random index
    lastRandomIndex: null,
    
    /*---DEFAULT FUNCTIONS---*/
    
    //Start the module
    init: function () {
      this.cacheDom();
      this.bindEvents();
      this.render();
    },
    
    //Cache all the necessary DOM elements
    cacheDom: function () {
      this.$el = $("#quotesModule");
      this.$tweetButt = this.$el.find("#tweetButt");
      this.$quoteButt = this.$el.find("#quoteButt");
      this.$quotes = this.$el.find("#quotes");
      this.template = this.$el.find("#quote-template").html();
    },
    
    //Bind all the events
    bindEvents: function () {
      this.$quoteButt.on("click", this.changeQuote.bind(this));
      this.$tweetButt.on("click", this.tweetQuote.bind(this));
    },
    
    //Render module dinamic HTML
    render: function () {
      var data = {
        quotes: this.quotes[this.generateRandomIndex()],
        author: quotes.author,
        quote: quotes.quote
      };
      this.$quotes.html(Mustache.render(this.template, data));
    },
    
    /*---CORE FUNCTIONS---*/
    
    /*--BINDED--*/
    
    //Change the quote displayed
    changeQuote: function () {
      this.render();
    },
    
    //Get the quote to post it on your Twitter account
    tweetQuote: function () {
      this.$tweetButt.attr("href", "https://twitter.com/intent/tweet?text=" + '"' + $(".quote").text() + '"' + $(".author").text());
    },
    
    /*--NOT-BINDED--*/
    
    //Generates and return a random index Number
    generateRandomIndex: function () {
      var nQuotes = this.quotes.length,
          newRandomIndex = Math.random() * nQuotes | 0;
      
      while (this.lastRandomIndex === newRandomIndex) {
        newRandomIndex = Math.random() * nQuotes | 0;
      }
      
      this.lastRandomIndex = newRandomIndex;
      return newRandomIndex;
    }
    
  };
  
  quotes.init();
}());